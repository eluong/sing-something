package fr.imac.singsomething.adapters;

import java.util.List;

import com.facebook.widget.ProfilePictureView;
import com.parse.ParseObject;
import com.parse.ParseUser;

import fr.imac.singsomething.DashboardActivity;
import fr.imac.singsomething.R;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RoundsListAdapter extends BaseAdapter {

	private static final int ITEM_VIEW_TYPE_ROUND_ACTIVE = 0;
	private static final int ITEM_VIEW_TYPE_ROUND_PENDING = 1;
	private static final int ITEM_VIEW_TYPE_SEPARATOR = 2;
	private static final int ITEM_VIEW_TYPE_NO_RESULTS = 3;
	private static final int ITEM_VIEW_TYPE_COUNT = 4;

	private List<Object> rounds = null;
	private ParseUser currentUser;
	private Context context;

	public RoundsListAdapter(Context context,List<Object> rounds, ParseUser user) {
		this.rounds = rounds;
		currentUser = user;
		this.context = context;
	}

	@Override
	public int getCount() {
		if (rounds !=null) return rounds.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return rounds.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return ITEM_VIEW_TYPE_COUNT;
	}

	@Override
	public int getItemViewType(int position) {

		if(rounds.get(position) == null) {
			return ITEM_VIEW_TYPE_NO_RESULTS;
		}
		else if(rounds.get(position) instanceof String) {
			return ITEM_VIEW_TYPE_SEPARATOR;
		} else {	
			ParseObject singingUser = (ParseObject) rounds.get(position);
			String singingUserId = singingUser.getParseObject("singingUser").getObjectId();

			if(singingUserId.equals(currentUser.getObjectId())) {
				return ITEM_VIEW_TYPE_ROUND_PENDING;
			} else {
				return ITEM_VIEW_TYPE_ROUND_ACTIVE;
			}	
		}

	}	

	@Override
	public boolean isEnabled(int position) {
		return getItemViewType(position) == ITEM_VIEW_TYPE_ROUND_ACTIVE;
	}	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int type = getItemViewType(position);

		View v = convertView;

		if (v==null) {

			int layout = -1;
			switch(type) {
			case ITEM_VIEW_TYPE_SEPARATOR :
				layout = R.layout.item_dashboard_title;
				break;
			case ITEM_VIEW_TYPE_ROUND_ACTIVE :
			case ITEM_VIEW_TYPE_ROUND_PENDING :
				layout = R.layout.item_dashboard;
				break;
			case ITEM_VIEW_TYPE_NO_RESULTS :
				layout = R.layout.item_dashboard_no_results;
				break;
			}
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(layout, parent, false);
		}

		if (type == ITEM_VIEW_TYPE_SEPARATOR) {

			TextView roundsListTitle =  (TextView) v.findViewById(R.id.rounds_list_title);
			roundsListTitle.setText((String) getItem(position));

		} else if(type != ITEM_VIEW_TYPE_NO_RESULTS) {
			ParseObject object = (ParseObject) getItem(position);

			TextView opponentNameTextView = (TextView) v.findViewById(R.id.opponent_name);
			TextView roundNumberTextView = (TextView) v.findViewById(R.id.round_number);
			ProfilePictureView picture = (ProfilePictureView) v.findViewById(R.id.opponent_picture);

			String roundNumber = Integer.toString(object.getInt("roundNumber"));
			roundNumberTextView.setText(roundNumber);

			ParseObject user = object.getParseObject((type == ITEM_VIEW_TYPE_ROUND_PENDING) ? "listeningUser" : "singingUser");

			String opponentName = user.getString("displayName");
			String facebookId = user.getString("facebookId");

			picture.setProfileId(facebookId);

			roundNumberTextView.setText(roundNumber);
			opponentNameTextView.setText(opponentName);

			v.setTag(R.id.user_id,user.getObjectId());
			v.setTag(R.id.user_name_id,opponentName);
			v.setTag(R.id.user_facebook_id,facebookId);

		}

		return v;
	}


}
