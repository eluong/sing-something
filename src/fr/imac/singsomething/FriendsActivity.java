package fr.imac.singsomething;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;

import fr.imac.singsomething.adapters.TabsAdapter;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.app.ActionBar;

/*
	Gestion des amis
*/

public class FriendsActivity extends FragmentActivity implements ActionBar.TabListener {

	private ActionBar actionBar;
	private ViewPager viewPager;
	private TabsAdapter tabsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Initialisation parse
		Parse.initialize(this, "RMuTFeIxMlaTzDuh5nE7mcSQVqXgi9n0apsa8N6I", "xmufdxSVC0yhZIZCGPvSHLHWgBJr52ED8zWBePUt"); 
		ParseFacebookUtils.initialize("1418683568364479");

		setContentView(R.layout.activity_friends);
		
		viewPager = (ViewPager) findViewById(R.id.pager);
		tabsAdapter = new TabsAdapter(getSupportFragmentManager());
		viewPager.setAdapter(tabsAdapter);
		viewPager.setOffscreenPageLimit(2);
		
		// Customization de l'action bar
		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setTitle(R.string.friends_action_bar_title);	
		actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS); 

        // Ajout des tabs
        actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.friends_tab_text)).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.request_tab_text)).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.add_friends_tab_text)).setTabListener(this));
        
        // Mise en evidence de la tab active
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        	 
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
         
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
         
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
            
        });
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.friends_menu, menu);
		return true;
	}    

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch(item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default :
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

}
