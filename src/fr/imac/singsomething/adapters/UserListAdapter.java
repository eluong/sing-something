package fr.imac.singsomething.adapters;

import java.util.List;

import com.facebook.widget.ProfilePictureView;
import com.parse.ParseObject;

import fr.imac.singsomething.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/* 
	Adapter pour le choix du chanteur collaborateur
 */

public class UserListAdapter extends BaseAdapter {

	private List<ParseObject> users = null;
	private LayoutInflater inflater;

	public UserListAdapter(Context context, List<ParseObject> users) {
		this.users = users;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		if (users !=null) return users.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return users.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;

		if (v==null) {
			v = inflater.inflate(R.layout.item_opponents_list, parent, false);
		}

		ParseObject friend = users.get(position).getParseObject("toUser");

		TextView friendNameTextView =  (TextView) v.findViewById(R.id.friend_name);
		TextView friendPointsNumberTextView = (TextView) v.findViewById(R.id.points_number);
		ProfilePictureView friendPicture = (ProfilePictureView) v.findViewById(R.id.friend_picture);

		String friendName = friend.getString("displayName");
		String friendFacebookId = friend.getString("facebookId");
		String pointsNumber = Integer.toString(friend.getInt("points")) + " " + v.getResources().getString(R.string.points).toString();

		friendPicture.setProfileId(friendFacebookId);
		friendNameTextView.setText(friendName);
		friendPointsNumberTextView.setText(pointsNumber);

		v.setTag(R.id.user_id,friend.getObjectId());
		v.setTag(R.id.user_name_id,friendName);
		v.setTag(R.id.user_facebook_id,friendFacebookId);

		return v;				

	}

}
