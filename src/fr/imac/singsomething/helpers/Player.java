package fr.imac.singsomething.helpers;

import java.io.FileDescriptor;
import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;

/*
	Classe pour gerer la lecture des musiques
 */

public class Player {

	private static MediaPlayer player = null;
	private String fileName = null;
	private FileDescriptor fileDescriptor = null;

	public Player(String fileName) {
		this.fileName = fileName;
	}

	public Player(FileDescriptor fileDescriptor) {
		this.fileDescriptor = fileDescriptor;
	}

	public void startPlaying() {
		
		player = new MediaPlayer();

		try {

			if(fileName != null) {
				player.setDataSource(fileName);
			}
			else if(fileDescriptor != null) {
				player.setDataSource(fileDescriptor);
			}

			player.prepare();
			player.start();
			
			Log.d("MyApp","playing");
		} catch (IOException e) {
			Log.d("MyApp",e.toString());
		}
	}

	public void setOnCompletionListener(OnCompletionListener listener) {
		player.setOnCompletionListener(listener);
	}

	public void stopPlaying() {
		if(player!=null) {
			player.release();
			player = null;
		}
	}

	public void release() {
		if(player!=null) {
			player.release();
			player = null;
		}
	}

}
