package fr.imac.singsomething.helpers;

import java.util.Comparator;

import com.parse.ParseObject;

/*
	Trie des amis en fonction des points decroissants
	Impossible a faire lors de la requete sur Parse car le nom est accessible dans la classe dont les infos sont recuperees via un pointeur
*/

public class FriendsPointsComparator implements Comparator<ParseObject> {

	// Trie en fonction des points decroissants
	@Override
	public int compare(ParseObject friend1, ParseObject friend2) {
		return friend2.getParseObject("toUser").getInt("points") - friend1.getParseObject("toUser").getInt("points");
	}

}
