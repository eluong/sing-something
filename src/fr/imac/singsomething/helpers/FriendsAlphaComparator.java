package fr.imac.singsomething.helpers;

import java.util.Comparator;

import com.parse.ParseObject;

/*
	Trie des amis par ordre alphabetique.
	Impossible a faire lors de la requete sur Parse car le nom est accessible dans la classe dont les infos sont recuperees via un pointeur
 */

public class FriendsAlphaComparator implements Comparator<ParseObject> {

	private String objectString; // toUser, fromUser, empty
	
	public FriendsAlphaComparator() {
		objectString = "";
	}
	
	public FriendsAlphaComparator(String s) {
		objectString = s;
	}
	
	// Trie par ordre alphabetique
	@Override
	public int compare(ParseObject friend1, ParseObject friend2) {
		
		int diff = 0;
		
		if(!objectString.equals("")) {
			diff = friend1.getParseObject(objectString).getString("displayName").compareTo(friend2.getParseObject(objectString).getString("displayName"));
		} else {
			diff = friend1.getString("displayName").compareTo(friend2.getString("displayName"));
		}
		
		return diff;
	}

}
