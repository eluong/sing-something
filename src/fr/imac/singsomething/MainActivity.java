package fr.imac.singsomething;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

/*
	Connexion de l'utilisateur via Facebook
*/

public class MainActivity extends Activity {	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		
		setContentView(R.layout.activity_main);
		
		// Initialisation parse
		Parse.initialize(this, "RMuTFeIxMlaTzDuh5nE7mcSQVqXgi9n0apsa8N6I", "xmufdxSVC0yhZIZCGPvSHLHWgBJr52ED8zWBePUt"); 
		ParseFacebookUtils.initialize("1418683568364479");

		// Chargement en cours
		final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		final ImageButton imageButton = (ImageButton) findViewById(R.id.facebook_connect);

		ParseUser currentUser = ParseUser.getCurrentUser();
		if ((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) { // Utilisateur deja connecte
			showDashboardActivity();
	    } else { // Connexion
	    	imageButton.setVisibility(View.VISIBLE);
			imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onLoginButtonClicked();
					progressBar.setVisibility(View.VISIBLE);
					imageButton.setVisibility(View.GONE);
				}
			});
	    }
	}


	private void onLoginButtonClicked() {
		// Connexion Facebook
		ParseFacebookUtils.logIn(this, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException err) {

				if (user == null) { // Erreur de connexion
					
					Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_try_again).toString(), Toast.LENGTH_SHORT);
					toast.show();
					finish();
					
				} else if (user.isNew()) { // Premiere connexion
					
					Session session = ParseFacebookUtils.getSession();
					if (session != null && session.isOpened()) {
						updateInfos(user);
					}
					showDashboardActivity();
					
				} else {
					
					showDashboardActivity();
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}

	// Affiche le tableau de bord
	private void showDashboardActivity() {
		Intent intent = new Intent(this, DashboardActivity.class);
		startActivity(intent);
		finish();
	}	

	// Mise a jour dans la base de donnees des infos sur l'utilisateur
	private void updateInfos(final ParseUser parseUser) {

		// Recupere le nom et l'id de l'utilisateur Facebook
		Bundle params = new Bundle();
		params.putString("fields", "id,name");

		// Requete Facebook
		Request req = Request.newMeRequest(ParseFacebookUtils.getSession(), new Request.GraphUserCallback() {
			@Override
			public void onCompleted(GraphUser user, Response response) {
				if (user != null) {     
					String facebookId = user.getId();
					String displayName = user.getName();

					// Remplissage des champs
					parseUser.put("points",0);
					parseUser.put("facebookId", facebookId);
					parseUser.put("displayName", displayName);  					
					parseUser.saveInBackground();

				}
			}
		});
		req.executeAsync();		
	}	
}
