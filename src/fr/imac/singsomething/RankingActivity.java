package fr.imac.singsomething;

import java.util.Collections;
import java.util.List;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import fr.imac.singsomething.adapters.RankingAdapter;
import fr.imac.singsomething.helpers.FriendsAlphaComparator;
import fr.imac.singsomething.helpers.FriendsPointsComparator;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/*
	Classement des joueurs
*/

public class RankingActivity extends Activity {

	private ParseUser currentUser;
	private ActionBar actionBar;	
	
	private ListView listView;
	private TextView textView;
	private ProgressBar progressBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_ranking);
		
		Parse.initialize(this, "RMuTFeIxMlaTzDuh5nE7mcSQVqXgi9n0apsa8N6I", "xmufdxSVC0yhZIZCGPvSHLHWgBJr52ED8zWBePUt"); 
		ParseFacebookUtils.initialize("1418683568364479");

		currentUser = ParseUser.getCurrentUser();
		
		// Customization de l'action bar
		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setTitle(R.string.rank);	
		actionBar.setDisplayShowCustomEnabled(true);
		
		listView = (ListView) findViewById(R.id.ranking_list);
		textView = (TextView) findViewById(R.id.error_text);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		
		setRank();
	}
	
	// Affiche le classement
	private void setRank() {
		
		progressBar.setVisibility(View.VISIBLE);
		listView.setVisibility(View.GONE);
		textView.setVisibility(View.GONE);
		
		final Activity activity = this;
		
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Friend");
		query.whereEqualTo("fromUser",currentUser);
		query.whereEqualTo("pending", false);
		query.include("toUser");
		
		// On cherche tous les amis du joueur
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if(e==null) {
					
					// On ajoute le joueur a la liste
					ParseObject user = ParseObject.create("Friend");
					try {
						currentUser.fetch();
					} catch (ParseException e1) {
					}
					user.put("toUser",currentUser);
					objects.add(user);
					
					// Tri par ordre alphabetique et par nombre de points
					Collections.sort(objects, new FriendsAlphaComparator("toUser"));
					Collections.sort(objects, new FriendsPointsComparator());
					
					RankingAdapter adapter = new RankingAdapter(activity, objects, currentUser);
					listView.setAdapter(adapter);
					textView.setVisibility(View.GONE);
					listView.setVisibility(View.VISIBLE);
				} else {
					textView.setText(R.string.error_refresh);
					textView.setVisibility(View.VISIBLE);
				}

				progressBar.setVisibility(View.GONE);
			}
		});		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.ranking_menu, menu);
		return true;
	}    
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		case R.id.refresh:
			setRank();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
