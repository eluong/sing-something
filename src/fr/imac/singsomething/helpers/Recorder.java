package fr.imac.singsomething.helpers;

import java.io.IOException;

import android.media.MediaRecorder;

/*
	Classe pour gerer l'enregistrement des musiques
*/

public class Recorder {

	private MediaRecorder recorder = null;
	private String fileName = null;
	
	public Recorder(String fileName) {
		this.fileName = fileName;
	}
	
	public void startRecording() {
		recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(fileName);
        
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		
        try {
            recorder.prepare();
        } catch (IOException e) {
        }
        recorder.start();
	}
	
	public void stopRecording() {
    	if(recorder!=null) {
            recorder.stop();
            recorder.release();
            recorder = null;	
    	}
	}
	
	public void release() {
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }
	}
}
