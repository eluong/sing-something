package fr.imac.singsomething.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

import fr.imac.singsomething.R;

/*
	Adapter pour une liste des categories de musique
*/

public class CategoriesListAdapter extends ParseQueryAdapter<ParseObject> {
	
	public CategoriesListAdapter(Activity context, ParseQueryAdapter.QueryFactory<ParseObject> query) {
		super(context,query);
	}

	@Override
	public View getItemView(ParseObject object, View v, ViewGroup parent) {
		if (v == null) {
			v = View.inflate(getContext(), R.layout.item_categories_list, null);
		}
		super.getItemView(object, v, parent);
		
		TextView categoryTextView = (TextView) v.findViewById(R.id.category);
		
		String categoryName = object.getString("name").toString();
		
		categoryTextView.setText(categoryName);

		v.setTag(R.id.category_id,object.getObjectId());
		v.setTag(R.id.category_name_id,categoryName);
		
		return v;
	}
}
