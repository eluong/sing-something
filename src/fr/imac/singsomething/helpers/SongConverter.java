package fr.imac.singsomething.helpers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/*
	Conversion des musiques
 */

public class SongConverter {
	
	// Tableau de bits en mp3
	public static FileDescriptor ByteArrayToSong(byte[] data, File fileDir) {
		
		FileDescriptor fd = null;
		
		try {
			File tempMp3 = File.createTempFile("temp", "mp3", fileDir);
			tempMp3.deleteOnExit();
			FileOutputStream fos = new FileOutputStream(tempMp3);
			fos.write(data);
			fos.close();
			FileInputStream fis = new FileInputStream(tempMp3);
			
			fd = fis.getFD();
		} catch (IOException e) {
		}
		
		return fd;
	}
	
	// Fichier son en tableau de bits
	public static byte[] SongToByteArray(String s)
	{
		byte[] byteArray = null;
		try
		{
			InputStream inputStream = new FileInputStream(s);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] b = new byte[1024*8];
			int bytesRead =0;

			while ((bytesRead = inputStream.read(b)) != -1)
			{
				bos.write(b, 0, bytesRead);
			}

			byteArray = bos.toByteArray();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return byteArray;
	}

}