package fr.imac.singsomething.adapters;

import com.parse.ParseObject;

import fr.imac.singsomething.R;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

/*
	Adapter pour une liste de musiques
*/

public class SongsListAdapter extends BaseAdapter {

	private ParseObject[] songsList;
	private LayoutInflater inflater;
	private Context context;
	
	public SongsListAdapter(Context context,ParseObject[] songsList) {
		this.songsList = songsList;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		if (songsList[0] != null && songsList[1] != null && songsList[2] != null) {
			return songsList.length;
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		 return songsList[position];
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;

		if (view == null) {
			view = inflater.inflate(R.layout.item_songs_list, parent, false);
		}

		ParseObject song = songsList[position];
		
		TextView songArtistTextView = (TextView) view.findViewById(R.id.song_artist);
		TextView songTitleTextView = (TextView) view.findViewById(R.id.song_title);
		TextView nbPointsTextView = (TextView) view.findViewById(R.id.nb_points);
		
		String songTitle = song.getString("title").toString();
		String songArtist = song.getString("artist").toString();
		final String youtubeId = song.getString("youtubeId").toString();
		
		int nbPoints = 0;
		
		switch(position) {
			case 0 :
				nbPoints = 10;
				break;
			case 1 :
				nbPoints = 20;
				break;
			case 2 :
				nbPoints = 30;
			break;
		}
				
		songArtistTextView.setText(songArtist);
		songTitleTextView.setText(songTitle);

		nbPointsTextView.setText(String.valueOf(nbPoints) + " " + view.getResources().getString(R.string.points).toString());

		view.setTag(R.id.nb_points_id, nbPoints);
		view.setTag(R.id.song_id, song.getObjectId());
		view.setTag(R.id.song_title_id, songTitle);
		view.setTag(R.id.song_artist_id, songArtist);
		
		ImageButton playYoutube = (ImageButton) view.findViewById(R.id.play_youtube);
		
		playYoutube.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + youtubeId));
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		});
		
		return view;
	}

}
