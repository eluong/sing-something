package fr.imac.singsomething.fragments;

import java.util.ArrayList;
import java.util.List;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import fr.imac.singsomething.R;
import fr.imac.singsomething.adapters.AddFriendsAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/*
	Retrouver les amis Facebook qui utilisent l'appli et effectuer des demandes d'ajout
*/

public class AddFriendsFragment extends Fragment {

	private ParseUser currentUser;
	private ProgressBar progressBar;
	private ListView listView;
	private TextView textView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_user_friends, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		final View parent = getView();

		setHasOptionsMenu(true);
		currentUser = ParseUser.getCurrentUser();
		progressBar = (ProgressBar) parent.findViewById(R.id.progress_user_friends);
		listView = (ListView) parent.findViewById(R.id.user_friends_list);
		textView = (TextView) parent.findViewById(R.id.no_results);	

		showList();
	}

	private void showList() {

		final View parent = getView();
		final Activity activity = getActivity();	

		progressBar.setVisibility(View.VISIBLE);
		listView.setVisibility(View.GONE);
		textView.setVisibility(View.GONE);

		// On recupere les amis Facebook du joueur
		Request request = Request.newMyFriendsRequest(ParseFacebookUtils.getSession(), new Request.GraphUserListCallback() {
			@Override
			public void onCompleted(List<GraphUser> users, Response response) { 

				if(users!=null) {

					// Liste des amis Facebook du joueur
					final List<String> facebookFriendsList = new ArrayList<String>();
					for (GraphUser user : users) {
						facebookFriendsList.add(user.getId());
					}

					// Liste des amis Parse et des amis Parse en attente
					ParseQuery<ParseObject> parseFriendsQuery = new ParseQuery<ParseObject>("Friend");
					parseFriendsQuery.whereEqualTo("fromUser",currentUser);

					// Liste des amis Parse a traiter
					ParseQuery<ParseObject> requestFriendsQuery = new ParseQuery<ParseObject>("Friend");
					requestFriendsQuery.whereEqualTo("toUser",currentUser);
					requestFriendsQuery.whereEqualTo("pending", true);

					// Combinaison des deux requetes qui recupere la liste des tous les amis a exclure de la liste
					List<ParseQuery<ParseObject>> excludeQueries = new ArrayList<ParseQuery<ParseObject>>();
					excludeQueries.add(parseFriendsQuery);
					excludeQueries.add(requestFriendsQuery);
					ParseQuery<ParseObject> mainExcludeQuery = ParseQuery.or(excludeQueries);

					final List<String> friendsToExclude = new ArrayList<String>();

					// On execute la requete pour trouver les amis a exclure de la liste
					mainExcludeQuery.findInBackground(new FindCallback<ParseObject>() {

						@Override
						public void done(List<ParseObject> objects, ParseException e) {

							if(e!=null) {
								manageError();
							} else {
								
								for(ParseObject friend : objects) {

									// Ajout des amis a exclure dans la liste
									ParseObject toUser = friend.getParseUser("toUser");
									ParseObject fromUser = friend.getParseUser("fromUser");

									if(toUser.getObjectId().equals(currentUser.getObjectId())) {

										try {
											friendsToExclude.add(fromUser.fetch().getString("facebookId"));
										} catch (ParseException e1) {
											manageError();
										}

									} else {

										try {
											friendsToExclude.add(toUser.fetch().getString("facebookId"));
										} catch (ParseException e1) {
											manageError();
										}

									}
								}

								// Requete finale
								ParseQuery<ParseUser> query = ParseUser.getQuery();
								query.whereContainedIn("facebookId", facebookFriendsList);
								query.whereNotContainedIn("facebookId", friendsToExclude);
								query.orderByAscending("displayName");
								query.findInBackground(new FindCallback<ParseUser>() {
									@Override
									public void done(List<ParseUser> objects, ParseException e) {									

										if (e == null) {

											if(objects.size()==0) { // Pas d'amis a afficher
												textView.setText(parent.getResources().getString(R.string.addfriends_no_results).toString());
												textView.setVisibility(View.VISIBLE);
												listView.setVisibility(View.GONE);
											} else {
												AddFriendsAdapter adapter = new AddFriendsAdapter(activity, objects, true);
												textView.setVisibility(View.GONE);
												listView.setAdapter(adapter);
												listView.setVisibility(View.VISIBLE);
											}

											progressBar.setVisibility(View.GONE);

										} else {
											manageError();
										}
									}
								});

							}

						}

					});

				} else {
					manageError();
				}

			}
		});
		request.executeAsync();


	}

	// Gestion des erreurs
	public void manageError() {
		textView.setText(R.string.error_refresh);
		textView.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.GONE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			showList();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
