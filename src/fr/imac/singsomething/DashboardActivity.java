package fr.imac.singsomething;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import fr.imac.singsomething.adapters.RoundsListAdapter;
import fr.imac.singsomething.helpers.CSVHelper;

/**
La classe DashboardActivity afficher les parties en cours et en attente
 */

public class DashboardActivity extends Activity {

	private ParseUser currentUser;
	private List<String> opponents; // Liste des identidiants de tous les adversaires ayant une partie en cours avec le joueur
	private Boolean firstQueryDone;  // Boolean qui s'assure que la deuxieme requete a ete effectuee, declanchant l'affichage du tableau de bord

	private ImageButton refreshDashboardButton;
	private ProgressBar progressBar;
	private ListView listView;
	private Button newGameButton;
	private TextView errorRefreshTextView;

	private List<Object> pendingRounds;
	private List<Object> activeRounds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_dashboard);
		
		// Initialisation parse
		Parse.initialize(this, "RMuTFeIxMlaTzDuh5nE7mcSQVqXgi9n0apsa8N6I", "xmufdxSVC0yhZIZCGPvSHLHWgBJr52ED8zWBePUt"); 
		ParseFacebookUtils.initialize("1418683568364479");

		initializeActionBar();

		currentUser = ParseUser.getCurrentUser();
		progressBar = (ProgressBar) findViewById(R.id.progress_dashboard);
		listView = (ListView) findViewById(R.id.games_list);
		newGameButton = (Button) findViewById(R.id.new_game);
		errorRefreshTextView = (TextView) findViewById(R.id.refresh_text);

		setDashboard();

		// Lancement d'une nouvelle partie
		newGameButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startNewGame();
			}
		});

		// A moi le micro
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String opponentId = view.getTag(R.id.user_id).toString();
				String opponentName = view.getTag(R.id.user_name_id).toString();
				String opponentFacebookId = view.getTag(R.id.user_facebook_id).toString();
				selectExistingGame(opponentId, opponentName, opponentFacebookId);
			}
		});

	};

	/* Initialisation du tableau de bord */
	public void setDashboard() {

		firstQueryDone = true;
		refreshDashboardButton.setClickable(false); // Bloquer le rafraichissement si la recuperation des parties est en cours
		opponents = new ArrayList<String>();

		errorRefreshTextView.setVisibility(View.GONE);
		progressBar.setVisibility(View.VISIBLE);
		listView.setVisibility(View.GONE);
		newGameButton.setVisibility(View.GONE);

		getRounds("pending");
		getRounds("active");
	}

	/* Recupere puis affiche une fois les requetes terminees, toutes les parties en cours et en attente */
	private void getRounds(String type) {

		// Qui joue quel role ?
		final String userRole = type.equals("pending") ? "singingUser" : "listeningUser";
		final String opponentRole = type.equals("active") ? "singingUser" : "listeningUser";

		// Requete
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Round");
		query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
		query.whereEqualTo(userRole,currentUser);
		query.whereEqualTo("isDone", false);
		query.orderByDescending("updatedAt");
		query.include(opponentRole);

		query.findInBackground( new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if(e == null) {					
					// Remplissage de la liste des id de tous les adversaires
					for(ParseObject o : objects) {
						opponents.add(o.getParseObject(opponentRole).getObjectId());
					}

					// Remplissage de la liste correspondante
					if(userRole.equals("singingUser")) {
						pendingRounds = new ArrayList<Object>();
						pendingRounds.add(getResources().getString(R.string.pending).toString());
						if(objects.size()!=0) {
							pendingRounds.addAll(objects);
						} else {
							pendingRounds.add(null);
						}
					} else {
						activeRounds = new ArrayList<Object>();
						activeRounds.add(getResources().getString(R.string.my_turn).toString());
						if(objects.size()!=0) {
							activeRounds.addAll(objects);
						} else {
							activeRounds.add(null);
						}
					}

					// Les deux requetes ont ete executees, on peut afficher
					if(!(firstQueryDone && !(firstQueryDone = false))) {

						activeRounds.addAll(pendingRounds);

						RoundsListAdapter adapter = new RoundsListAdapter(getApplicationContext(), activeRounds, currentUser);
						listView.setAdapter(adapter);
						listView.setVisibility(View.VISIBLE);
						progressBar.setVisibility(View.GONE);
						newGameButton.setVisibility(View.VISIBLE);

						firstQueryDone = true;
						refreshDashboardButton.setClickable(true);
					}

				} else {
					// On affiche le message d'erreur
					if(!(firstQueryDone && !(firstQueryDone = false))) {
						progressBar.setVisibility(View.GONE);
						errorRefreshTextView.setVisibility(View.VISIBLE);
						firstQueryDone = true;
						refreshDashboardButton.setClickable(true);					
					}

				}
			}
		});
	}	

	/* Affiche la liste des amis */
	private void showUserProfile() {
		Intent intent = new Intent(this, UserProfileActivity.class);
		startActivity(intent);
	}		

	/* Commence une nouvelle partie */
	private void startNewGame() {
		Intent intent = new Intent(this, GameActivity.class);
		intent.putExtra("isNewGame", true);
		if(opponents.size() != 0) {
			String opponentsString = CSVHelper.IdsListToCSV(opponents);
			// Liste des adversaires en cours a ne pas afficher dans les adversaires choisissables
			intent.putExtra("opponentsList",opponentsString);	
		}
		startActivity(intent);
		this.finish();
	}

	/* Selectionner une partie en cours */
	private void selectExistingGame(String opponentId, String opponentName, String opponentFacebookId) {
		Intent intent = new Intent(this, GameActivity.class);
		intent.putExtra("isNewGame", false);
		intent.putExtra("opponentId", opponentId);
		intent.putExtra("opponentName", opponentName);
		intent.putExtra("opponentFacebookId", opponentFacebookId);
		startActivity(intent);
		this.finish();
	}

	/* Initialise l'action bar */
	private void initializeActionBar() {
		final ActionBar actionBar = getActionBar();

		LayoutInflater li = LayoutInflater.from(this);
		View actionBarView = li.inflate(R.layout.action_bar_dashboard, null);

		actionBar.setCustomView(actionBarView);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);

		ImageButton showUserProfile = (ImageButton) actionBarView.findViewById(R.id.show_user_profile);
		showUserProfile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showUserProfile();
			}
		});

		refreshDashboardButton = (ImageButton) actionBarView.findViewById(R.id.refresh);
		refreshDashboardButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				setDashboard();
			}
		});

	}
}
