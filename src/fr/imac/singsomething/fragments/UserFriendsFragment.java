package fr.imac.singsomething.fragments;

import java.util.Collections;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import fr.imac.singsomething.R;
import fr.imac.singsomething.adapters.UserFriendsAdapter;
import fr.imac.singsomething.helpers.FriendsAlphaComparator;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/*
	Liste des amis de l'utilisateur
*/

public class UserFriendsFragment extends Fragment {

	private ParseUser currentUser;
	private ProgressBar progressBar;
	private ListView listView;
	private TextView textView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_user_friends, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		setHasOptionsMenu(true);
		currentUser = ParseUser.getCurrentUser();

		final View parent = getView();
		progressBar = (ProgressBar) parent.findViewById(R.id.progress_user_friends);
		listView = (ListView) parent.findViewById(R.id.user_friends_list);
		textView = (TextView) parent.findViewById(R.id.no_results);
		
		showList();
	}

	// Affichage la liste des amis
	private void showList() {

		final View parent = getView();
		final Activity activity = getActivity();	

		progressBar.setVisibility(View.VISIBLE);
		listView.setVisibility(View.GONE);
		textView.setVisibility(View.GONE);

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Friend");
		query.whereEqualTo("fromUser",currentUser);
		query.include("toUser");

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if(e==null) {
					if(objects.size()==0) { // Pas d'amis
						textView.setText(parent.getResources().getString(R.string.userfriends_no_results).toString());
						textView.setVisibility(View.VISIBLE);
						listView.setVisibility(View.GONE);
					} else {
						Collections.sort(objects, new FriendsAlphaComparator("toUser"));
						UserFriendsAdapter adapter = new UserFriendsAdapter(activity, objects);
						listView.setAdapter(adapter);
						textView.setVisibility(View.GONE);
						listView.setVisibility(View.VISIBLE);
					}

				} else {
					textView.setText(R.string.error_refresh);
					textView.setVisibility(View.VISIBLE);
				}

				progressBar.setVisibility(View.GONE);
			}
		});		

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			showList();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}