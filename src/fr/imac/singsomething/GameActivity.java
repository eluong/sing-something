package fr.imac.singsomething;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.facebook.widget.ProfilePictureView;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;
import com.parse.ParseException;

import fr.imac.singsomething.adapters.CategoriesListAdapter;
import fr.imac.singsomething.adapters.SongsListAdapter;
import fr.imac.singsomething.adapters.UserListAdapter;
import fr.imac.singsomething.helpers.CSVHelper;
import fr.imac.singsomething.helpers.FriendsAlphaComparator;
import fr.imac.singsomething.helpers.LevenshteinDistance;
import fr.imac.singsomething.helpers.Player;
import fr.imac.singsomething.helpers.Recorder;
import fr.imac.singsomething.helpers.SongConverter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.AdapterView.OnItemClickListener;

/*
	Activite gerant la creation de parties
 */
public class GameActivity extends Activity {

	ActionBar actionBar;

	// Gestion des etapes
	private ViewFlipper switcher;
	private static final int LOADING_LAYOUT = 0;
	private static final int RESULTS_LAYOUT = 1;
	private static final int GUESS_LAYOUT = 2;
	private static final int RESULT_CURRENT_LAYOUT = 3;
	private static final int OPPONENT_LAYOUT = 4;
	private static final int THEME_LAYOUT = 5;
	private static final int SONG_LAYOUT = 6;
	private static final int RECORD_LAYOUT = 7;

	private ParseUser currentUser;
	private Boolean isNewGame;

	// Round en cours
	private ParseObject currentRound = null;

	// Informations le nouveau round
	private String opponentId;
	private String opponentName;
	private String opponentFacebookId;
	private String categoryId;
	private String categoryName;
	private String songId;
	private String songTitle;
	private String songArtist;
	private int roundNb;
	private int nbPoints;

	// Gestion de l'enregistrement et l'ecoute
	private boolean startPlaying = true;
	private Recorder recorder = null;
	private Player player = null;	
	private byte[] recording = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		// Garde l'ecran allume
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_game);

		// Initialisation parse
		Parse.initialize(this, "RMuTFeIxMlaTzDuh5nE7mcSQVqXgi9n0apsa8N6I", "xmufdxSVC0yhZIZCGPvSHLHWgBJr52ED8zWBePUt"); 
		ParseFacebookUtils.initialize("1418683568364479");
		
		// Son au max
		AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        
		actionBar = getActionBar();
		actionBar.setTitle(R.string.initializing_game);

		currentUser = ParseUser.getCurrentUser();
		switcher = (ViewFlipper) findViewById(R.id.ViewSwitcher); // Flipper qui gere les etapes du jeu

		Bundle extras = getIntent().getExtras();
		isNewGame = extras.getBoolean("isNewGame");

		if(isNewGame) { // Nouvelle partie

			roundNb = 1; // Initialisation du numero du round

			// On fait en sorte que le joueur selectionne un adversaire avec qui il n'a pas de partie en cours
			String opponentsString = extras.getString("opponentsList",null);
			List<ParseObject> opponents = new ArrayList<ParseObject>();
			if(opponentsString != null) {
				List<String> opponentsListString = CSVHelper.CSVToIdsList(opponentsString);
				for(String id : opponentsListString) {
					opponents.add(ParseObject.createWithoutData(ParseUser.class,id));
				}
			}
			chooseOpponent(opponents); // On redirige vers l'etape de choix de l'adversaire

		} else { // Partie existante avec un joueur

			// On recupere les informations de l'adversaire
			opponentId = extras.getString("opponentId",null);
			opponentName = extras.getString("opponentName","");
			opponentFacebookId = extras.getString("opponentFacebookId","");

			// On affiche les resultats du round precedent
			showPreviousRoundResults();
		}

	}


	/** 
		Type LAYOUT. Affiche les resultats du round precedent
	 */
	void showPreviousRoundResults() {		

		// Loading UI
		final ScrollView srollView = (ScrollView) findViewById(R.id.previous_results_wrapper);
		final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_guess);

		// Resultats de l'adversaire au round precedent
		ParseQuery<ParseObject> lastRoundQuery = ParseQuery.getQuery("Round");
		lastRoundQuery.whereEqualTo("singingUser", currentUser);
		ParseObject opponent = ParseObject.createWithoutData(ParseUser.class, opponentId);
		lastRoundQuery.whereEqualTo("listeningUser", opponent);
		lastRoundQuery.whereEqualTo("isDone", true);
		lastRoundQuery.include("song");

		lastRoundQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if(e==null && objects.size()==1) { // Round precedent trouve (Round >= 2)

					final ParseObject object = objects.get(0);

					actionBar.setTitle(getResources().getString(R.string.current_round_results).toString() + " " + Integer.toString(object.getInt("roundNumber")));

					// Infos adversaire
					ProfilePictureView opponentProfilePictureView = (ProfilePictureView) findViewById(R.id.opponent_picture_previous_results);
					TextView opponentPreviousResultsTextView = (TextView) findViewById(R.id.opponent_previous_results);
					TextView previousSuggestionArtistTextView =  (TextView) findViewById(R.id.previous_suggestion_artist);
					TextView previousSuggsetionTitleTextView =  (TextView) findViewById(R.id.previous_suggestion_title);

					opponentProfilePictureView.setProfileId(opponentFacebookId);
					opponentPreviousResultsTextView.setText(opponentName + " " + getResources().getString(R.string.opponent_suggested).toString());

					String answerArtist = (object.getString("answerArtist").equals("")) ? getResources().getString(R.string.no_suggestion).toString() : object.getString("answerArtist");
					String answerTitle = (object.getString("answerTitle").equals("")) ? getResources().getString(R.string.no_suggestion).toString() : object.getString("answerTitle");
					previousSuggestionArtistTextView.setText(answerArtist);
					previousSuggsetionTitleTextView.setText(answerTitle);

					// Infos joueur
					ProfilePictureView userProfilePictureView = (ProfilePictureView) findViewById(R.id.user_picture_previous_results);
					TextView previousRefArtistTextView =  (TextView) findViewById(R.id.previous_ref_artist);
					TextView previousRefTitleTextView =  (TextView) findViewById(R.id.previous_ref_title);

					userProfilePictureView.setProfileId(currentUser.getString("facebookId"));
					previousRefArtistTextView.setText(object.getParseObject("song").getString("artist"));
					previousRefTitleTextView.setText(object.getParseObject("song").getString("title"));

					int percentMatch = object.getInt("percentMatch");
					final int points = (int) Math.ceil( ( (float) percentMatch / 100.0 ) * object.getInt("points") );

					// Resultats
					TextView previousRoundResults = (TextView) findViewById(R.id.previous_round_results);
					previousRoundResults.setText(Html.fromHtml(
							getResources().getString(R.string.round_result_01).toString() + " <b>" + percentMatch + "%</b>" +
									getResources().getString(R.string.round_result_02).toString() + " <b>" + points + " " +
									getResources().getString(R.string.round_result_03).toString() + "</b>"
							)
							);

					switcher.setDisplayedChild(RESULTS_LAYOUT);

					// Passage a l'etape suivante
					final Button nextStep = (Button) findViewById(R.id.next_step);
					nextStep.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							nextStep.setEnabled(false);
							srollView.setVisibility(View.GONE);
							progressBar.setVisibility(View.VISIBLE);
							object.deleteInBackground();
							if(points != 0) {
								currentUser.increment("points", points);
								currentUser.saveInBackground();
							}
							guessSong();
						}
					});

				}
				else if(e==null && objects.size()==0) { // Pas de round precedent trouvee (Round < 2)
					guessSong();
				}
				else { // Erreur
					manageError();
				}
			}
		});
	}

	/** 
		Type LAYOUT. Le joueur tente de deviner la musique envoyee par l'adversaire
	 */	
	private void guessSong() {

		final Activity activity = this;

		final int LOAD = 0;
		final int GUESS_SCREEN = 1;

		final ViewFlipper viewFlipperGuess = (ViewFlipper) findViewById(R.id.ViewFlipperGuess);
		viewFlipperGuess.setMeasureAllChildren(false);
		viewFlipperGuess.setDisplayedChild(GUESS_SCREEN);

		final EditText playerSuggestionArtistEditText = (EditText) findViewById(R.id.player_suggestion_artist);
		final EditText playerSuggestionTitleEditText = (EditText) findViewById(R.id.player_suggestion_title);
		
		// Recuperation du round en cours
		ParseQuery<ParseObject> currentRoundQuery = ParseQuery.getQuery("Round");
		currentRoundQuery.whereEqualTo("listeningUser", currentUser);
		ParseObject opponent = ParseObject.createWithoutData(ParseUser.class, opponentId);
		currentRoundQuery.whereEqualTo("singingUser", opponent);
		currentRoundQuery.include("theme");
		currentRoundQuery.include("song");

		currentRoundQuery.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {

				if(e==null) {

					// Information concernant le round
					currentRound = object;
					roundNb = object.getInt("roundNumber") + 1;

					final int currentRoundNbPoints = object.getInt("points");
					final String themeName = object.getParseObject("theme").getString("name");
					final int themeId = object.getParseObject("theme").getInt("theme_id");

					// Si le joueur a deja donne sa reponse, il peut creer un nouveau round
					if(object.getBoolean("hasAnswer")) {
						chooseCategory();
						return;
					}

					// On recupere l'enregistrement
					ParseFile songFile = (ParseFile) object.get("record");
					songFile.getDataInBackground(new GetDataCallback() {

						@Override
						public void done(byte[] data, ParseException e) {

							if(e == null) {

								File tempMp3 = null;

								final String fileNameTest = Environment.getExternalStorageDirectory().getAbsolutePath() + "/testtet.3gp";
								File f = new File(fileNameTest);

								try {

									// Ecritures des donnes dans le fichier son
									FileOutputStream fos = new FileOutputStream(f);
									fos.write(data);
									fos.close();

									player = new Player(fileNameTest);

									switcher.setDisplayedChild(GUESS_LAYOUT);
									actionBar.setTitle(getResources().getString(R.string.round_nb).toString() + " " + Integer.toString(roundNb-1));

									String wordingArtist = getResources().getString(R.string.text_artist).toString();
									String wordingTitle = getResources().getString(R.string.text_title).toString();

									// Trouver le bon wording en fonction du theme
									switch(themeId) {
									case 1 : // Pour le theme Britney, on demande l'album au lieu de l'artiste
										wordingArtist = getResources().getString(R.string.text_album).toString();
										break;
									case 2 :
										break;
									case 3 : // Pour le theme Disney, on demande le film au lieu de l'artiste
										wordingArtist = getResources().getString(R.string.text_movie).toString();
										break;
									case 4 : // Pour le theme Classque, on demane le compositeur au lieu de l'artiste
										wordingArtist = getResources().getString(R.string.text_compositor).toString();
										break;
									default:
										break;
									}
									
									playerSuggestionArtistEditText.setHint(wordingArtist);
									playerSuggestionTitleEditText.setHint(wordingTitle);

									// Entete
									ProfilePictureView friendPicture = (ProfilePictureView) findViewById(R.id.opponent_picture_guess);
									friendPicture.setProfileId(opponentFacebookId);

									TextView guessInfos = (TextView) findViewById(R.id.guess_infos);
									guessInfos.setText(Html.fromHtml(getResources().getString(R.string.guess_infos_01).toString() + " <b>"
											+ opponentName + "</b> " +
											getResources().getString(R.string.guess_infos_02).toString() + " " +
											currentRoundNbPoints + " " + getResources().getString(R.string.guess_infos_03).toString()
											));		

									TextView themeTextView = (TextView) findViewById(R.id.guess_theme);
									themeTextView.setText(themeName);

								} catch (IOException e1) { // Erreur lors de la lecture du fichier
									manageError();
								}	

							} else {
								manageError();
							}
						}
					});
				} else {
					manageError();
				}

			} 
		});

		// Ecoute de l'enregistrement de l'adversaire
		final ImageButton listenOpponentRecord = (ImageButton) findViewById(R.id.opponent_record);
		startPlaying = true;
		listenOpponentRecord.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (startPlaying) {
					listenOpponentRecord.setImageResource(R.drawable.stop);
					player.startPlaying();

				} else {
					listenOpponentRecord.setImageResource(R.drawable.play);
					player.stopPlaying();

				}

				startPlaying = !startPlaying;
			}

		});

		// Passage a l'etape suivate : commencer un nouveau round
		final Button nextStep = (Button) findViewById(R.id.submit_guess_song);
		nextStep.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(player!=null) player.release();
				
				nextStep.setEnabled(false);

				viewFlipperGuess.setDisplayedChild(LOAD);

				final String suggestionArtist = playerSuggestionArtistEditText.getText().toString();
				final String suggestionTitle = playerSuggestionTitleEditText.getText().toString();

				final String refArtist = currentRound.getParseObject("song").getString("artist");
				final String refTitle = currentRound.getParseObject("song").getString("title");

				final int percentMatch = LevenshteinDistance.compareResults(refArtist, refTitle, suggestionArtist, suggestionTitle);
				final int points = (int) Math.ceil( ( (float) percentMatch / 100.0 ) * currentRound.getInt("points") );

				// Modification en ligne du round en cours
				if(currentRound!=null) {
					currentRound.put("answerTitle",suggestionArtist);
					currentRound.put("answerArtist",suggestionTitle);
					currentRound.put("percentMatch",percentMatch);
					currentRound.put("hasAnswer", true);
					currentRound.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							if(e==null) {
								if(points != 0) {
									currentUser.increment("points", points);
									currentUser.saveInBackground();
								}
								// Resultats du round
								showCurrentRoundResults(suggestionArtist, suggestionTitle, percentMatch, points);
							} else {
								manageError();
							}
						}
					});
				}
			}

		});


	}


	/** 
	Type LAYOUT. Affichage des resultats du round en cours
	 */	
	private void showCurrentRoundResults(String suggestionArtist, String suggestionTitle, int percentMatch, int points) {

		actionBar.setTitle(getResources().getString(R.string.current_round_results).toString() + " " + Integer.toString(roundNb - 1));

		// Infos adversaire
		ProfilePictureView opponentProfilePictureView = (ProfilePictureView) findViewById(R.id.opponent_picture_current_results);
		TextView opponentCurrentResultsTextView = (TextView) findViewById(R.id.opponent_current_results);
		TextView currentRefArtistTextView =  (TextView) findViewById(R.id.current_ref_artist);
		TextView currentRefTitleTextView =  (TextView) findViewById(R.id.current_ref_title);

		opponentProfilePictureView.setProfileId(opponentFacebookId);
		opponentCurrentResultsTextView.setText(opponentName + " " + getResources().getString(R.string.opponent_sang).toString());
		currentRefArtistTextView.setText(currentRound.getParseObject("song").getString("artist"));
		currentRefTitleTextView.setText(currentRound.getParseObject("song").getString("title"));

		// Infos joueur
		ProfilePictureView userProfilePictureView = (ProfilePictureView) findViewById(R.id.user_picture_current_results);
		TextView currentSuggestionArtistTextView =  (TextView) findViewById(R.id.current_suggestion_artist);
		TextView currentSuggestionTitleTextView =  (TextView) findViewById(R.id.current_suggestion_title);

		userProfilePictureView.setProfileId(currentUser.getString("facebookId"));
		currentSuggestionArtistTextView.setText( (suggestionArtist.equals("")) ? getResources().getString(R.string.no_suggestion).toString() : suggestionArtist );
		currentSuggestionTitleTextView.setText( (suggestionTitle.equals("")) ? getResources().getString(R.string.no_suggestion).toString() : suggestionTitle );

		// Resultats
		TextView currentRoundResults = (TextView) findViewById(R.id.current_round_results);
		currentRoundResults.setText(Html.fromHtml(
				getResources().getString(R.string.round_result_01).toString() + " <b>" + percentMatch + "%</b>" +
						getResources().getString(R.string.round_result_02).toString() + " <b>" + points + " " +
						getResources().getString(R.string.round_result_03).toString() + "</b>"
				)
				);

		switcher.setDisplayedChild(RESULT_CURRENT_LAYOUT);

		// Passage au round suivant
		Button createNewRound = (Button) findViewById(R.id.create_new_round);	
		createNewRound.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				chooseCategory();
			}
		});


	}

	/** 
		Type LAYOUT. Choix de l'adversaire. Cette etape est uniquement possible s'il s'agit d'une nouvelle partie.
	 */	
	private void chooseOpponent(final List<ParseObject> opponents) {

		switcher.setDisplayedChild(OPPONENT_LAYOUT);
		actionBar.setTitle(R.string.choose_opponent);

		final ProgressBar progressFriendsList = (ProgressBar) findViewById(R.id.progress_friendslist);
		final ListView listViewFriendsList = (ListView) findViewById(R.id.friends_list);
		final LinearLayout noAvailableOpponentsLayout = (LinearLayout) findViewById(R.id.no_available_opponents_layout);
		final Button backButton = (Button) findViewById(R.id.add_friends);

		final Activity activity = this;

		noAvailableOpponentsLayout.setVisibility(View.GONE);
		progressFriendsList.setVisibility(View.VISIBLE);
		listViewFriendsList.setVisibility(View.GONE);

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				restartDashboardActivity();
			}
		});


		// Requete
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Friend");
		query.whereEqualTo("fromUser",currentUser);
		if(opponents!=null) query.whereNotContainedIn("toUser", opponents);
		query.whereEqualTo("pending", false);
		query.include("toUser");

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if(e==null) {

					if(objects.size()==0) { // Pas de chanteur disponible
						listViewFriendsList.setVisibility(View.GONE);
						noAvailableOpponentsLayout.setVisibility(View.VISIBLE);
					} else { // Affichage des joueurs
						Collections.sort(objects, new FriendsAlphaComparator("toUser"));
						UserListAdapter adapter = new UserListAdapter(activity, objects);
						listViewFriendsList.setAdapter(adapter);
						listViewFriendsList.setVisibility(View.VISIBLE);
						noAvailableOpponentsLayout.setVisibility(View.GONE);
					}

					progressFriendsList.setVisibility(View.GONE);		

				} else {
					manageError();
				}

			}
		});


		// Choix de l'adversaire
		listViewFriendsList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				opponentId = view.getTag(R.id.user_id).toString();
				opponentName = view.getTag(R.id.user_name_id).toString();
				opponentFacebookId = view.getTag(R.id.user_facebook_id).toString();
				// Choix de la categorie
				chooseCategory();
				return;
			}
		});
	}


	/** 
		Type LAYOUT. Choix de la categorie
	 */	
	private void chooseCategory() {

		switcher.setDisplayedChild(THEME_LAYOUT);
		actionBar.setTitle(R.string.choose_category);

		final ProgressBar progressCategoriesList = (ProgressBar) findViewById(R.id.progress_categorieslist);
		final ListView listViewCategoriesList = (ListView) findViewById(R.id.categories_list);

		CategoriesListAdapter adapter = new CategoriesListAdapter(this, new ParseQueryAdapter.QueryFactory<ParseObject>() {
			public ParseQuery<ParseObject> create() {
				ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Theme");
				query.orderByAscending("name");
				return query;
			}
		});

		// Gestion du loader
		adapter.addOnQueryLoadListener(new OnQueryLoadListener<ParseObject>() {
			@Override
			public void onLoading() {
				progressCategoriesList.setVisibility(View.VISIBLE);
				listViewCategoriesList.setVisibility(View.GONE);
			}
			@Override
			public void onLoaded(List<ParseObject> objects, Exception e) {

				if(e!=null) {
					manageError();
				} else {
					progressCategoriesList.setVisibility(View.GONE);
					listViewCategoriesList.setVisibility(View.VISIBLE);		
				}
			}
		});	

		listViewCategoriesList.setAdapter(adapter);			

		// Choix de la categorie
		listViewCategoriesList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				categoryId = view.getTag(R.id.category_id).toString();
				categoryName = view.getTag(R.id.category_name_id).toString();
				chooseSong();
			}
		});

	}


	/** 
		Type LAYOUT. Choix de la musique
	 */	
	private void chooseSong() {

		switcher.setDisplayedChild(SONG_LAYOUT);
		actionBar.setTitle(R.string.choose_song);

		final ListView listViewSongsList = (ListView) findViewById(R.id.songs_list);
		final ProgressBar progressSongsList = (ProgressBar) findViewById(R.id.progress_songslist);
		progressSongsList.setVisibility(View.VISIBLE);
		listViewSongsList.setVisibility(View.GONE);		

		//  On recupere une musique par niveau et on les place dans une liste
		ParseObject[] songsList = new ParseObject[3];

		getSong("easy",songsList,progressSongsList,listViewSongsList);
		getSong("intermediate",songsList,progressSongsList,listViewSongsList);
		getSong("advanced",songsList,progressSongsList,listViewSongsList);

		// Choix de la musique
		listViewSongsList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				songId = view.getTag(R.id.song_id).toString();
				nbPoints = Integer.parseInt(view.getTag(R.id.nb_points_id).toString());
				songTitle = view.getTag(R.id.song_title_id).toString();
				songArtist = view.getTag(R.id.song_artist_id).toString();

				sing();
			}
		});		
	}


	/** 
		Recuperation des musiques suivant le niveau demande
	 */		
	private void getSong(final String level, final ParseObject[] songsList , final ProgressBar progressSongsList, final ListView listViewSongsList) {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Song");
		ParseObject theme = ParseObject.createWithoutData("Theme",this.categoryId);
		query.whereEqualTo("theme",theme);
		query.whereEqualTo("level",level);

		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e != null) {
					manageError();
				} else {
					int id = 0;
					if(level.equals("easy")) id=0;
					else if(level.equals("intermediate")) id=1;
					else if(level.equals("advanced")) id=2;

					Random r = new Random();
					songsList[id] = objects.get(r.nextInt(objects.size()));

					if(songsList[0] != null && songsList[1] != null && songsList[2] != null) { // Il existe une musique par niveau
						// Inflate
						SongsListAdapter adapter = new SongsListAdapter(getApplicationContext(), songsList);
						listViewSongsList.setAdapter(adapter);

						progressSongsList.setVisibility(View.GONE);
						listViewSongsList.setVisibility(View.VISIBLE);
					}
				}
			}
		});
	}


	/** 
		Type LAYOUT. C'est enfin au joueur de changer
	 */	
	private void sing() {

		switcher.setDisplayedChild(RECORD_LAYOUT);
		actionBar.setTitle(getResources().getString(R.string.round_nb).toString() + " " + Integer.toString(roundNb));

		// Entete
		ProfilePictureView friendPicture = (ProfilePictureView) findViewById(R.id.opponent_picture);
		TextView roundInfos = (TextView) findViewById(R.id.round_infos);
		friendPicture.setProfileId(opponentFacebookId);
		roundInfos.setText(Html.fromHtml(getResources().getString(R.string.round_infos_01).toString() + " <b>"
				+ songTitle + "</b> de <b>" + songArtist +
				"</b> " + getResources().getString(R.string.round_infos_03).toString() + " <b>" + opponentName + "</b>. " +
				nbPoints + " " + getResources().getString(R.string.round_infos_04).toString()
				));		


		// Enregistreur et lecteur
		final String fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/singsomething_temp.3gp";
		recorder = new Recorder(fileName);		
		player = new Player(fileName);

		// Flipper
		final int START_RECORDING = 0;
		final int COUNTDOWN = 1;
		final int SUBMIT_RECORDING = 2;
		final int LOAD = 3;
		final ViewFlipper viewFlipperRecord = (ViewFlipper) findViewById(R.id.viewFlipperRecord);
		viewFlipperRecord.setMeasureAllChildren(false);

		viewFlipperRecord.setDisplayedChild(START_RECORDING);

		// Enregistrement en cours
		final TextView countdownTextView = (TextView) findViewById(R.id.countdown_timer);
		final CountDownTimer recordingCountDownTimer = new CountDownTimer(21000, 100) { // 20 secondes

			@Override
			public void onTick(long millisUntilFinished) {
				countdownTextView.setText("00:" + String.format("%02d", millisUntilFinished / 1000));
			}

			@Override
			public void onFinish() {
				recorder.stopRecording();
				recorder.release();
				viewFlipperRecord.setDisplayedChild(SUBMIT_RECORDING);
			}
		};

		// Demarrer l'enregistrement
		final ImageButton startRecordingButton = (ImageButton) findViewById(R.id.start_recording_image);
		startRecordingButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				recordingCountDownTimer.start();
				recorder.startRecording();
				Log.d("MyApp","I clicked !!");
				viewFlipperRecord.setDisplayedChild(COUNTDOWN);

			}
		});


		// Ecouter ce qu'on a enregistre (la honte !)
		final ImageButton playButton =  (ImageButton) findViewById(R.id.listen_recording);
		playButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (startPlaying) {
					player.startPlaying();

					player.setOnCompletionListener(new OnCompletionListener() {        
						@Override
						public void onCompletion(MediaPlayer player) {

							playButton.setImageResource(R.drawable.play);
							startPlaying = !startPlaying;

						}
					});

				} else {
					player.stopPlaying();
				}

				if (startPlaying) {
					playButton.setImageResource(R.drawable.stop);
				} else {
					playButton.setImageResource(R.drawable.play);
				}

				startPlaying = !startPlaying;
			}

		});

		// Soumettre l'enregistrement et creer le round
		final Button submitButton = (Button) findViewById(R.id.submit_round);
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(player!=null)  player.release();
				recording = SongConverter.SongToByteArray(fileName);
				viewFlipperRecord.setDisplayedChild(LOAD);
				createRound();
			}
		});

	}


	/** 
		Creation du round sur Parse
	 */	
	private void createRound() {

		final Activity activity = this;

		// Etape 1 : Enregistrement du fichier
		final ParseFile recordFile = new ParseFile(recording);
		recordFile.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if(e != null) {

					manageError();

				} else {

					// Creation de la partie
					final ParseObject round = new ParseObject("Round");

					round.put("roundNumber",roundNb);
					round.put("singingUser", currentUser);
					ParseObject listeningUser = ParseObject.createWithoutData(ParseUser.class, opponentId);
					round.put("listeningUser", listeningUser);
					ParseObject category = ParseObject.createWithoutData("Theme", categoryId);
					round.put("theme", category);
					ParseObject song = ParseObject.createWithoutData("Song", songId);
					round.put("song", song);
					round.put("hasAnswer", false);
					round.put("isDone", false);
					round.put("points", nbPoints);
					round.put("record",recordFile);

					// Enregistrement du round
					round.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {

							if(e == null) {

								// Modification du round en cours, il est termine !
								if(!isNewGame) {
									currentRound.put("isDone", true);
									currentRound.saveInBackground(new SaveCallback() {

										@Override
										public void done(ParseException e) {
											if(e==null) {
												restartDashboardActivity();
												if(player!=null) {
													player.release();
												}
											} else {
												try {
													round.delete(); // En cas d'erreur, on supprime le nouveau round
													restartDashboardActivity();
												} catch (ParseException e1) {
													manageError();
												}
											}
										}
									});
								} else {
									restartDashboardActivity();
								}

							} else {
								// Le nouveau round ne s'est pas sauvegarde
								manageError();
							}
						}
					});
				} 
			}
		}); 
	}


	/** 
		Gestion des erreurs
	 */	
	private void manageError() {
		Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.game_error).toString(), Toast.LENGTH_SHORT);
		toast.show();
		restartDashboardActivity();
	}


	/** 
		Redemarre l'activite tableau de bord
	 */	
	private void restartDashboardActivity() {
		Intent intent = new Intent(this, DashboardActivity.class);
		startActivity(intent);
		finish();

	}

	// Bouton retour : confirmation de fin de partie
	@Override
	public void onBackPressed() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final AlertDialog alert = builder.create();

		LayoutInflater inflater = getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirm, null);

		Button yesButton = (Button) dialogView.findViewById(R.id.yes_button);
		Button noButton = (Button) dialogView.findViewById(R.id.no_button);

		// Quitter la partie
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				restartDashboardActivity();  

			}
		});

		// Ne pas quitter la partie
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				alert.dismiss();
			}
		});

		alert.setView(dialogView);
		alert.show();
	}


	@Override
	public void onPause() {
		super.onPause();

		if(recorder!=null) {
			recorder.release();
		}

		if(player!=null) {
			player.release();
		}

	}
}
