package fr.imac.singsomething.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.ProfilePictureView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import fr.imac.singsomething.R;

/*
Adapter pour la liste des demandes d'ajout d'amis
 */

public class FriendsRequestAdapter extends BaseAdapter {

	private List<ParseObject> friends = null;
	private LayoutInflater inflater;
	private ParseUser currentUser;
	private Context context;

	public FriendsRequestAdapter(Context context, List<ParseObject> friends, Boolean showPoints) {
		this.friends = friends;
		this.context = context;
		currentUser = ParseUser.getCurrentUser();
		inflater = LayoutInflater.from(context);
	}	

	@Override
	public int getCount() {
		if (friends !=null) return friends.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return friends.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;

		if (v==null) {
			v = inflater.inflate(R.layout.item_friends_request, parent, false);
		}

		final ParseObject object = friends.get(position);
		final ParseObject friend = object.getParseObject("fromUser");

		final TextView friendNameTextView =  (TextView) v.findViewById(R.id.friend_name);
		ProfilePictureView friendPicture = (ProfilePictureView) v.findViewById(R.id.friend_picture);
		final ImageButton statusButton = (ImageButton) v.findViewById(R.id.image_status);
		final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
		final LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.image_wrapper);
		
		progressBar.setVisibility(View.GONE);

		String friendName = friend.getString("displayName");
		String friendFacebookId = friend.getString("facebookId");
		final String addedSentence = friendName + " " + v.getResources().getString(R.string.added_friend).toString();

		friendPicture.setProfileId(friendFacebookId);
		friendNameTextView.setText(friendName);

		statusButton.setOnClickListener(new OnClickListener() { // Accepte l'ami quand on clique sur le bouton

			@Override
			public void onClick(View v) {

				statusButton.setClickable(false);
				progressBar.setVisibility(View.VISIBLE);
				statusButton.setVisibility(View.GONE);

				object.put("pending", false);
				object.saveInBackground(new SaveCallback() {

					@Override
					public void done(ParseException e) { // Modification de la relation existante

						if(e==null) {

							// Creation de la relation dans l'autre sens
							ParseObject reverseRelation = new ParseObject("Friend");
							reverseRelation.put("fromUser",currentUser);
							reverseRelation.put("toUser", friend);
							reverseRelation.put("pending", false);
							reverseRelation.saveInBackground(new SaveCallback() {

								@Override
								public void done(ParseException e) {

									if(e==null) {

										progressBar.setVisibility(View.GONE);
										statusButton.setVisibility(View.GONE);
										linearLayout.setVisibility(View.GONE);
										friendNameTextView.setText(addedSentence);
										
										
									} else { // Erreur

										object.put("pending", true);
										object.saveInBackground();

										progressBar.setVisibility(View.GONE);
										statusButton.setVisibility(View.VISIBLE);
										statusButton.setClickable(true);
										Toast toast = Toast.makeText(context, R.string.error_try_again, Toast.LENGTH_SHORT);
										toast.show();

									}

								}
							});
						} else { // Erreur

							progressBar.setVisibility(View.GONE);
							statusButton.setVisibility(View.VISIBLE);
							statusButton.setClickable(true);
							Toast toast = Toast.makeText(context, R.string.error_try_again, Toast.LENGTH_SHORT);
							toast.show();

						}
					}
				});

			}
		});

		return v;				
	}

}
