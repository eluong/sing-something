package fr.imac.singsomething.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.widget.ProfilePictureView;
import com.parse.ParseObject;
import com.parse.ParseUser;

import fr.imac.singsomething.R;

/*
	Adapter pour la liste des amis
 */

public class UserFriendsAdapter extends BaseAdapter {
	
	private List<ParseObject> friends = null;
	private LayoutInflater inflater;
	private ParseUser currentUser;
	Context context;
	
	public UserFriendsAdapter(Context context, List<ParseObject> friends) {
		this.friends = friends;
		this.context = context;
		currentUser = ParseUser.getCurrentUser();
		inflater = LayoutInflater.from(context);
	}	
	
	@Override
	public int getCount() {
		if (friends !=null) return friends.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return friends.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;

		if (v==null) {
			v = inflater.inflate(R.layout.item_user_friends, parent, false);
		}

		final ParseObject object = friends.get(position);
		final ParseObject friend = object.getParseObject("toUser");
		
		TextView friendNameTextView =  (TextView) v.findViewById(R.id.friend_name);
		ProfilePictureView friendPicture = (ProfilePictureView) v.findViewById(R.id.friend_picture);
		TextView pendingTextView =  (TextView) v.findViewById(R.id.pending);
		
		String friendName = friend.getString("displayName");
		String friendFacebookId = friend.getString("facebookId");
		
		friendPicture.setProfileId(friendFacebookId);
		friendNameTextView.setText(friendName);

		if(object.getBoolean("pending")==true) {
			pendingTextView.setText(v.getResources().getString(R.string.friend_pending));
			pendingTextView.setVisibility(View.VISIBLE);
		} else {
			pendingTextView.setVisibility(View.GONE);
		}
		
		return v;				
	}

}
