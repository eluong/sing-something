package fr.imac.singsomething.adapters;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.ProfilePictureView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import fr.imac.singsomething.R;

/*
	Adapter pour l'ajout d'un amis Facebook
 */

public class AddFriendsAdapter extends BaseAdapter {

	private List<ParseUser> friends = null;
	private LayoutInflater inflater;
	private ParseUser currentUser;
	private Context context;

	public AddFriendsAdapter(Context context, List<ParseUser> friends, Boolean showPoints) {
		this.friends = friends;
		this.context = context;
		currentUser = ParseUser.getCurrentUser();
		inflater = LayoutInflater.from(context);
	}	

	@Override
	public int getCount() {
		if (friends !=null) return friends.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return friends.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;

		if (v==null) {
			v = inflater.inflate(R.layout.item_add_friends, parent, false);
		}

		final ParseObject friend = friends.get(position);

		final TextView friendNameTextView =  (TextView) v.findViewById(R.id.friend_name);
		ProfilePictureView friendPicture = (ProfilePictureView) v.findViewById(R.id.friend_picture);
		final ImageButton statusButton = (ImageButton) v.findViewById(R.id.image_status);
		final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
		final LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.image_wrapper);
		
		progressBar.setVisibility(View.GONE);

		String friendName = friend.getString("displayName");
		String friendFacebookId = friend.getString("facebookId");
		final String addedSentence = v.getResources().getString(R.string.added_to_pending).toString() + " " + friendName + ".";

		
		friendPicture.setProfileId(friendFacebookId);
		friendNameTextView.setText(friendName);

		statusButton.setOnClickListener(new OnClickListener() { // Ajoute l'ami quand on clique sur le bouton

			@Override
			public void onClick(View v) {

				statusButton.setClickable(false);
				progressBar.setVisibility(View.VISIBLE);
				statusButton.setVisibility(View.GONE);

				String userId = friend.getObjectId();
				final ParseObject user = ParseObject.createWithoutData(ParseUser.class, userId);
				final ParseObject friendRelation = new ParseObject("Friend");
				friendRelation.put("fromUser",currentUser);
				friendRelation.put("toUser", friend);
				friendRelation.put("pending",true);
				friendRelation.saveInBackground(new SaveCallback() { // Ajout de l'ami

					@Override
					public void done(ParseException e) {

						if(e==null) {
							
							progressBar.setVisibility(View.GONE);
							statusButton.setVisibility(View.GONE);
							linearLayout.setVisibility(View.GONE);
							friendNameTextView.setText(addedSentence);
							
						} else { // Erreur

							progressBar.setVisibility(View.GONE);
							statusButton.setVisibility(View.VISIBLE);
							statusButton.setClickable(true);
							Toast toast = Toast.makeText(context, R.string.error_try_again, Toast.LENGTH_SHORT);
							toast.show();

						}
					}
				});

			}
		});



		return v;				
	}

}
