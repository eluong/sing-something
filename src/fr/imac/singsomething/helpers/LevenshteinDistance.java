package fr.imac.singsomething.helpers;

import java.text.Normalizer;

/*
  Classe qui calcule le pourcentage de correspondance entre la response proposee par le joueur et la bonne reponse
 */

public class LevenshteinDistance {
	private static int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	// Calcul de la distance de Levenshtein
	private static int computeLevenshteinDistance(String str1,String str2) {
		int[][] distance = new int[str1.length() + 1][str2.length() + 1];

		for (int i = 0; i <= str1.length(); i++)
			distance[i][0] = i;
		for (int j = 1; j <= str2.length(); j++)
			distance[0][j] = j;

		for (int i = 1; i <= str1.length(); i++)
			for (int j = 1; j <= str2.length(); j++)
				distance[i][j] = minimum(
						distance[i - 1][j] + 1,
						distance[i][j - 1] + 1,
						distance[i - 1][j - 1]+ ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0 : 1));

		return distance[str1.length()][str2.length()];    
	}
	
	// Formate une string, on les mets en minuscules et on supprime tous les accents
	private static String normalizeString(String s) {
		
		s = s.toLowerCase();
		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[^\\p{ASCII}]", "");
		
		return s;
	}	
	
	// Fonction qui calcul le pourcentage de correspondance
	public static int compareResults(String aRef, String tRef, String aSug, String tSug) {

		aRef = normalizeString(aRef);
		tRef = normalizeString(tRef);
		aSug = normalizeString(aSug);
		tSug = normalizeString(tSug);
		
		// Correspondance pour l'artiste
		float resultArtist = (float) ((1.0 - ((float) computeLevenshteinDistance(aRef, aSug) / (float)  Math.max(aRef.length(), aSug.length()))) * 100.0);
		
		// Correspondance pour le titre
		float resultTitle = (float) ((1.0 - ((float) computeLevenshteinDistance(tRef, tSug) / (float) Math.max(tRef.length(), tSug.length()))) * 100.0);
		
		// Moyenne des deux
		return (int) ((resultArtist + resultTitle) / 2);
	}

	
}
