package fr.imac.singsomething;

import com.facebook.widget.ProfilePictureView;
import com.parse.ParseUser;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/*
	Page profil de l'utilisateur
*/

public class UserProfileActivity extends Activity {

	private ParseUser currentUser;

	private ProgressBar progressBar;
	private LinearLayout header;
	private LinearLayout buttons;
	private LinearLayout error;
	private ProfilePictureView userPicture;
	private TextView userName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_user_profile);

		// Customization de l'action bar
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setTitle(R.string.user_profile);	
		actionBar.setDisplayShowCustomEnabled(true);

		currentUser = ParseUser.getCurrentUser();

		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		header = (LinearLayout) findViewById(R.id.header);
		buttons = (LinearLayout) findViewById(R.id.buttons);
		error = (LinearLayout) findViewById(R.id.error);

		Button friendsButton = (Button) findViewById(R.id.friends);
		Button rankButton = (Button) findViewById(R.id.rank);
		Button logoutButton = (Button) findViewById(R.id.logout);

		userPicture = (ProfilePictureView) findViewById(R.id.user_picture);
		userName = (TextView) findViewById(R.id.user_name);

		// Gestion des amis
		friendsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startFriendsActivity();
			}
		});

		// Voir le classement
		rankButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startRankingActivity();
			}
		});

		// Se deconnecter
		logoutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				logOut();
			}
		});

		setProfile();
	}

	// Affichage du profil
	private void setProfile() {

		progressBar.setVisibility(View.VISIBLE);
		header.setVisibility(View.GONE);
		buttons.setVisibility(View.GONE);
		error.setVisibility(View.GONE);

		userName.setText(currentUser.getString("displayName"));
		userPicture.setProfileId(currentUser.getString("facebookId"));

		progressBar.setVisibility(View.GONE);
		header.setVisibility(View.VISIBLE);
		buttons.setVisibility(View.VISIBLE);
		error.setVisibility(View.GONE);


	}

	// Affichage du classement
	private void startRankingActivity() {
		Intent intent = new Intent(this, RankingActivity.class);
		startActivity(intent);
	}

	// Gestion des amis
	private void startFriendsActivity() {
		Intent intent = new Intent(this, FriendsActivity.class);
		startActivity(intent);
	}

	// Deconnexion
	private void logOut() {
		ParseUser.logOut();		
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("finish", true);
		// On vide l'historique des activites
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | 
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
}
