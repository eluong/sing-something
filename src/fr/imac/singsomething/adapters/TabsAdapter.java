package fr.imac.singsomething.adapters;

import fr.imac.singsomething.fragments.AddFriendsFragment;
import fr.imac.singsomething.fragments.FriendsRequestFragment;
import fr.imac.singsomething.fragments.UserFriendsFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/* 
	Adapter pour la gestion des tabs pour la gestion des amis
 */

public class TabsAdapter extends FragmentPagerAdapter {

	public TabsAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {

        switch (arg0) {
        case 0:
            return new UserFriendsFragment();
        case 1:
            return new FriendsRequestFragment();
        case 2:
            return new AddFriendsFragment();
        }
        
		return null;
	}

	@Override
	public int getCount() {
		return 3;
	}

}
