package fr.imac.singsomething.helpers;

import java.util.Arrays;
import java.util.List;

public class CSVHelper {

	// Convertit une liste de strings au format CSV
	public static String IdsListToCSV(List<String> ids) {
		String csv = ids.toString().replace("[", "").replace("]", "").replace(", ", ",");
		return csv;
	}
	
	// Convertie une string CVS en liste
	public static List<String> CSVToIdsList(String csv) {
		List<String> list = Arrays.asList(csv.split("\\s*,\\s*"));
		return list;
	}
	
}
