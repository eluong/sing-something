package fr.imac.singsomething.adapters;

import java.util.List;

import com.facebook.widget.ProfilePictureView;
import com.parse.ParseObject;
import com.parse.ParseUser;

import fr.imac.singsomething.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
	Adapter pour le classement des joueurs
 */

public class RankingAdapter extends BaseAdapter {

	private List<ParseObject> users = null;
	private LayoutInflater inflater;
	private ParseUser currentUser;

	public RankingAdapter(Context context, List<ParseObject> users, ParseUser user) {
		this.users = users;
		this.currentUser = user;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		if (users !=null) return users.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return users.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;

		if (v==null) {
			v = inflater.inflate(R.layout.item_ranking, parent, false);
		}

		ParseObject friend = users.get(position).getParseObject("toUser");

		TextView friendNameTextView =  (TextView) v.findViewById(R.id.friend_name);
		TextView rankNumberTextView = (TextView) v.findViewById(R.id.rank_number);
		TextView friendPointsNumberTextView = (TextView) v.findViewById(R.id.points_number);
		ProfilePictureView friendPicture = (ProfilePictureView) v.findViewById(R.id.friend_picture);
		LinearLayout rankNumberLayout = (LinearLayout) v.findViewById(R.id.rank_number_wrapper);

		if(friend.equals(currentUser)) {
			rankNumberLayout.setBackgroundResource(R.drawable.orange_gradient);
		}

		String friendName = friend.getString("displayName");
		String friendFacebookId = friend.getString("facebookId");
		String pointsNumber = Integer.toString(friend.getInt("points")) + " " + v.getResources().getString(R.string.points);

		friendPicture.setProfileId(friendFacebookId);
		friendNameTextView.setText(friendName);
		rankNumberTextView.setText(Integer.toString(position+1));
		friendPointsNumberTextView.setText(pointsNumber);

		return v;				

	}

}
